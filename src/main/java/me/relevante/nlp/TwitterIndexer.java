package me.relevante.nlp;

import me.relevante.model.TwitterSearchUser;
import me.relevante.network.Twitter;
import me.relevante.persistence.NetworkIndexRepo;
import me.relevante.persistence.NetworkSearchUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TwitterIndexer extends AbstractNetworkIndexer<Twitter, TwitterSearchUser, TwitterStemUserIndex>
        implements NetworkIndexer<Twitter> {

    @Autowired
    public TwitterIndexer(final TwitterSearchUserCreator searchUserCreator,
                          final TwitterVectorizer vectorizer,
                          final NetworkIndexRepo<Twitter, TwitterStemUserIndex> indexRepo,
                          final NetworkSearchUserRepo<Twitter, TwitterSearchUser> searchUserRepo) {
        super(searchUserCreator, vectorizer, indexRepo, searchUserRepo);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected TwitterStemUserIndex createIndexEntry(String stem, String userId, double score) {
        return new TwitterStemUserIndex(stem, userId, score);
    }
}
