package me.relevante.nlp;

import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterStemUserIndex")
public class TwitterStemUserIndex extends AbstractNetworkStemUserIndex<Twitter> implements NetworkStemUserIndex<Twitter> {

    public TwitterStemUserIndex(String stem, String userId, double score) {
        super(stem, userId, score);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

}
