package me.relevante.nlp;

import me.relevante.model.NetworkSignal;
import me.relevante.model.NetworkSignalExtractor;
import me.relevante.model.TwitterActionFav;
import me.relevante.model.TwitterActionReply;
import me.relevante.model.TwitterFullPost;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterSearchUser;
import me.relevante.network.Twitter;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TwitterSearchUserCreator extends AbstractNetworkSearchUserCreator<Twitter, TwitterFullUser, TwitterFullPost, TwitterSearchUser, TwitterActionFav, TwitterActionReply>
        implements NetworkSearchUserCreator<Twitter, TwitterSearchUser> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Autowired
    public TwitterSearchUserCreator(NetworkSignalExtractor<Twitter, TwitterFullUser, TwitterFullPost> signalExtractor,
                                    NetworkVectorizer<Twitter> vectorizer,
                                    CrudRepository<TwitterFullUser, String> fullUserRepo,
                                    NetworkFullPostRepo<TwitterFullPost> fullPostRepo,
                                    NetworkPostActionRepo<Twitter, TwitterActionFav> likesRepo,
                                    NetworkPostActionRepo<Twitter, TwitterActionReply> commentsRepo) {
        super(signalExtractor, vectorizer, fullUserRepo, fullPostRepo, likesRepo, commentsRepo);
    }

    @Override
    public void addLikesAndCommentsToFullPosts(List<TwitterActionFav> likes,
                                               List<TwitterActionReply> comments,
                                               Map<String, TwitterFullPost> fullPostMap) {
        for (TwitterActionFav like : likes) {
            TwitterFullPost fullPost = fullPostMap.get(like.getPostId());
            fullPost.getFavs().add(like);
        }
        for (TwitterActionReply comment : comments) {
            TwitterFullPost fullPost = fullPostMap.get(comment.getPostId());
            fullPost.getReplies().add(comment);
        }
    }

    @Override
    public TwitterSearchUser createSearchUser(TwitterFullUser fullUser, List<NetworkSignal<Twitter>> signals) {
        TwitterSearchUser searchUser = new TwitterSearchUser(fullUser);
        searchUser.getSignals().addAll(signals);
        return searchUser;
    }

    @Override
    protected void addCustomSignals(TwitterFullUser fullUser, List<NetworkSignal<Twitter>> list) { }
}
