package me.relevante.nlp;

import me.relevante.model.TwitterSignalBioDescription;
import me.relevante.model.TwitterSignalDescription;
import me.relevante.model.TwitterSignalPostAuthorship;
import me.relevante.model.TwitterSignalPostFav;
import me.relevante.model.TwitterSignalPostReply;
import me.relevante.model.TwitterSignalPostRetweet;
import me.relevante.network.Twitter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class TwitterVectorizer extends AbstractNetworkVectorizer<Twitter> implements NetworkVectorizer<Twitter> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected Map<Class, Function<Double, Double>> createWeightFunctionsBySignal() {
        Map<Class, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(TwitterSignalDescription.class, n -> 12.5);
        weightFunctionsBySignal.put(TwitterSignalBioDescription.class, n -> 12.5);
        weightFunctionsBySignal.put(TwitterSignalPostAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostRetweet.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostFav.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostReply.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        return weightFunctionsBySignal;
    }
}
