package me.relevante.api;

import me.relevante.model.TwitterPost;
import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;

import java.util.List;

/**
 * Created by daniel-ibanez on 16/07/16.
 */
public interface TwitterApi extends NetworkApi<Twitter, TwitterProfile> {

    TwitterProfile getUserData() throws TwitterApiException;
    TwitterProfile getUserData(String userId) throws TwitterApiException;
    List<TwitterProfile> getFriends(String userId) throws TwitterApiException;
    List<TwitterPost> getTimeline(String userId) throws TwitterApiException;
    TwitterProfile getProfileByScreenName(String screenName) throws TwitterApiException;
    TwitterPost getTweet(String id) throws TwitterApiException;
    List<TwitterPost> getSearchResults(List<String> searchTerms) throws TwitterApiException;
    TwitterApiResponse follow(String userId) throws TwitterApiException;
    TwitterApiResponse sendDirectMessage(String userId, String message) throws TwitterApiException;
    TwitterApiResponse replyPost(String postId, String screenName, String text) throws TwitterApiException;
    TwitterApiResponse retweet(String tweetId) throws TwitterApiException;
    TwitterApiResponse favTweet(String tweetId) throws TwitterApiException;

}
