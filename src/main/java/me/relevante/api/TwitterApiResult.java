package me.relevante.api;

public enum TwitterApiResult {

    ALREADY_PROCESSED,
    EXCEPTION,
    FORBIDDEN,
    SUCCESS,
    UNDEFINED_ERROR

}
