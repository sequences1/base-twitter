package me.relevante.api;

import twitter4j.RateLimitStatus;
import twitter4j.TwitterException;

/**
 * Created by daniel-ibanez on 16/07/16.
 */
public class TwitterApiException extends RuntimeException {

    private TwitterException e;

    public TwitterApiException() {
        this.e = new TwitterException("");
    }

    public TwitterApiException(String message) {
        this.e = new TwitterException(message);
    }

    public TwitterApiException(TwitterException e) {
        this.e = e;
    }

    public String getMessage() {
        return e.getMessage();
    }

    public int getStatusCode() {
        return e.getStatusCode();
    }

    public int getErrorCode() {
        return e.getErrorCode();
    }

    public String getResponseHeader(String name) {
        return e.getResponseHeader(name);
    }

    public RateLimitStatus getRateLimitStatus() {
        return e.getRateLimitStatus();
    }

    public int getAccessLevel() {
        return e.getAccessLevel();
    }

    public int getRetryAfter() {
        return e.getRetryAfter();
    }

    public boolean isCausedByNetworkIssue() {
        return e.isCausedByNetworkIssue();
    }

    public boolean exceededRateLimitation() {
        return e.exceededRateLimitation();
    }

    public boolean resourceNotFound() {
        return e.resourceNotFound();
    }

    public String getExceptionCode() {
        return e.getExceptionCode();
    }

    @Override
    public String toString() {
        return "TwitterApiException{" +
                "e=" + (e == null ? "" : e.toString()) +
                "} " + super.toString();
    }
}
