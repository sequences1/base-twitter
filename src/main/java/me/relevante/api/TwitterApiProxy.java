package me.relevante.api;

import me.relevante.model.TwitterPost;
import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TwitterApiProxy implements TwitterApi {

    private static final Logger logger = LoggerFactory.getLogger(TwitterApiProxy.class);

    private TwitterApiAdapter mainTwitterApiAdapter;
    private TwitterApiAdapter extraTwitterApiAdapter;
    private TwitterApiAdapter emergencyTwitterApiAdapter;
    private LinkedList<TwitterApiAdapter> readPool;
    private LinkedList<TwitterApiAdapter> writePool;

    public TwitterApiProxy(OAuthKeyPair<Twitter> oAuthMainConsumerKeyPair,
                           OAuthKeyPair<Twitter> oAuthExtraConsumerKeyPair,
                           OAuthKeyPair<Twitter> oAuthEmergencyConsumerKeyPair,
                           OAuthTokenPair<Twitter> oAuthAccessToken) {
        this.mainTwitterApiAdapter = new TwitterApiAdapter(oAuthMainConsumerKeyPair, oAuthAccessToken);
        this.extraTwitterApiAdapter = new TwitterApiAdapter(oAuthExtraConsumerKeyPair, oAuthAccessToken);
        this.emergencyTwitterApiAdapter = new TwitterApiAdapter(oAuthEmergencyConsumerKeyPair, oAuthAccessToken);
        this.readPool = new LinkedList<>(Arrays.asList(mainTwitterApiAdapter, extraTwitterApiAdapter, emergencyTwitterApiAdapter));
        this.writePool = new LinkedList<>(Arrays.asList(mainTwitterApiAdapter, extraTwitterApiAdapter));
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public TwitterProfile getUserData() throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            return apiAdapter.getUserData();
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getUserData();
        }
    }

    @Override
    public TwitterProfile getUserData(String userId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            return apiAdapter.getUserData(userId);
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getUserData();
        }
    }

    @Override
    public List<TwitterProfile> getFriends(String userId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            List<TwitterProfile> profiles = apiAdapter.getFriends(userId);
            return profiles;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getFriends(userId);
        }
    }

    @Override
    public List<TwitterPost> getTimeline(String userId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            List<TwitterPost> tweets = apiAdapter.getTimeline(userId);
            return tweets;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getTimeline(userId);
        }
    }

    @Override
    public TwitterProfile getProfileByScreenName(String screenName) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            TwitterProfile profile = apiAdapter.getProfileByScreenName(screenName);
            return profile;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getUserData();
        }
    }

    @Override
    public TwitterPost getTweet(String tweetId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            TwitterPost tweet = apiAdapter.getTweet(tweetId);
            return tweet;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getTweet(tweetId);
        }
    }

    @Override
    public List<TwitterPost> getSearchResults(List<String> searchTerms) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = readPool.get(0);
        try {
            List<TwitterPost> tweets = apiAdapter.getSearchResults(searchTerms);
            return tweets;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getSearchResults(searchTerms);
        }
    }

    @Override
    public TwitterApiResponse follow(String userId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.follow(userId);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return follow(userId);
        }
    }

    @Override
    public TwitterApiResponse sendDirectMessage(String userId, String message) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.sendDirectMessage(userId, message);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return sendDirectMessage(userId, message);
        }
    }

    @Override
    public TwitterApiResponse replyPost(String postId, String screenName, String text) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.replyPost(postId, screenName, text);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return replyPost(postId, screenName, text);
        }
    }

    @Override
    public TwitterApiResponse retweet(String tweetId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.retweet(tweetId);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return retweet(tweetId);
        }
    }

    @Override
    public TwitterApiResponse favTweet(String tweetId) throws TwitterApiException {
        TwitterApiAdapter apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.favTweet(tweetId);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return favTweet(tweetId);
        }
    }

    private void processReadTwitterApiException(TwitterApiException e) throws TwitterApiException {
        if (e.getRetryAfter() >= 0) {
            waitUntilApiIsAvailable(e.getRetryAfter());
            return;
        }
        TwitterApiAdapter currentTwitterApiAdapter = readPool.get(0);
        this.readPool.remove(currentTwitterApiAdapter);
        this.writePool.remove(currentTwitterApiAdapter);
        if (readPool.isEmpty()) {
            throw new TwitterApiException(e.getMessage());
        }
    }

    private void processWriteTwitterApiException(TwitterApiException e) throws TwitterApiException {
        if (e.getRetryAfter() >= 0) {
            waitUntilApiIsAvailable(e.getRetryAfter());
            return;
        }
        this.writePool.remove(0);
        if (writePool.isEmpty()) {
            throw new TwitterApiException(e.getMessage());
        }
    }

    private void waitUntilApiIsAvailable(int retryAfter) {
        long millisToSleep = retryAfter * 1000;
        try {
            Thread.sleep(millisToSleep);
        } catch (InterruptedException e) {
            logger.error("Error", e);
        }
    }
}


