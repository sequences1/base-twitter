package me.relevante.model;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.stereotype.Component;

@Component
public class TwitterSearchUserListener extends AbstractMongoEventListener<TwitterSearchUser> {

    @Override
    public void onAfterConvert(AfterConvertEvent<TwitterSearchUser> event) {
        super.onAfterConvert(event);
        TwitterSearchUser searchUser = event.getSource();
        for (NetworkSignal signal : searchUser.getSignals()) {
            signal.setRelatedUserId(searchUser.getId());
        }
    }
}
