package me.relevante.model;

import me.relevante.network.Network;
import me.relevante.network.Twitter;

import java.util.Date;

public class TwitterFav implements NetworkEntity {

    private long authorId;
    private long tweetId;
    private Date creationTimestamp;

    private TwitterFav() {
    }

    public TwitterFav(long authorId,
                      long tweetId,
                      Date creationTimestamp) {
        this.authorId = authorId;
        this.tweetId = tweetId;
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public Network getNetwork() {
        return Twitter.getInstance();
    }

    public String getAuthorId() {
        return String.valueOf(authorId);
    }

    public String getPostId() {
        return String.valueOf(tweetId);
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    public Long getAuthorTwitterId() {
        return authorId;
    }

    public Long getPostTwitterId() {
        return tweetId;
    }

}
