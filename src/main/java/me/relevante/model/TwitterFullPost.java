package me.relevante.model;

import me.relevante.network.Network;
import me.relevante.network.Twitter;
import me.relevante.nlp.core.Keyword;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "TwitterFullPost")
public class TwitterFullPost implements NetworkFullPost<Twitter, TwitterPost> {

    @Id
    private String id;
    private TwitterPost post;
    private List<Keyword> keywords;

    @Transient
    private List<TwitterActionFav> favs;
    @Transient
    private List<TwitterActionRetweet> retweets;
    @Transient
    private List<TwitterActionReply> replies;

    private TwitterFullPost() {
        this.keywords = new ArrayList<>();
        this.favs = new ArrayList<>();
        this.retweets = new ArrayList<>();
        this.replies = new ArrayList<>();
    }

    public TwitterFullPost(TwitterPost post) {
        this();
        this.id = post.getId();
        this.post = post;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public TwitterPost getPost() {
        return post;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public List<TwitterActionFav> getFavs() {
        return favs;
    }

    public void setFavs(List<TwitterActionFav> favs) {
        this.favs = favs;
    }

    public List<TwitterActionRetweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(List<TwitterActionRetweet> retweets) {
        this.retweets = retweets;
    }

    public List<TwitterActionReply> getReplies() {
        return replies;
    }

    public void setReplies(List<TwitterActionReply> replies) {
        this.replies = replies;
    }

    public List<TwitterActionReply> findRepliesByAuthorId(String authorId) {
        List<TwitterActionReply> replies = new ArrayList<>();
        if (authorId == null) {
            return replies;
        }
        for (TwitterActionReply reply : this.replies) {
            if (authorId.equals(reply.getAuthorId())) {
                replies.add(reply);
            }
        }
        return replies;
    }

    public List<TwitterActionRetweet> findRetweetsByAuthorId(String authorId) {
        List<TwitterActionRetweet> retweets = new ArrayList<>();
        if (authorId == null) {
            return retweets;
        }
        for (TwitterActionRetweet retweet : this.retweets) {
            if (authorId.equals(retweet.getAuthorId())) {
                retweets.add(retweet);
            }
        }
        return retweets;
    }

    public List<TwitterActionFav> findFavsByAuthorId(String authorId) {
        List<TwitterActionFav> favs = new ArrayList<>();
        if (authorId == null) {
            return favs;
        }
        for (TwitterActionFav fav : this.favs) {
            if (authorId.equals(fav.getAuthorId())) {
                favs.add(fav);
            }
        }
        return favs;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        TwitterFullPost that = (TwitterFullPost) obj;
        return this.id.equals(that.id);
    }


}
