package me.relevante.model;

import java.util.Date;

public class TwitterSignalPostFav extends TwitterBaseSignal implements NetworkSignal {

    public TwitterSignalPostFav() {
        super("PF", SignalCategory.CONTENT_ACTIVITY);
    }

    public TwitterSignalPostFav(TwitterPost post,
                                String commentAuthorId,
                                Date commentTimestamp) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = commentAuthorId;
        this.signalTimestamp = (commentTimestamp != null) ? commentTimestamp : new Date();
    }
}
