package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterActionReply")
public class TwitterActionReply extends AbstractNetworkPostAction<Twitter> implements NetworkPostAction<Twitter> {

    private String replyText;

    private TwitterActionReply() {
        super();
    }

    public TwitterActionReply(String authorId,
                              String tweetId,
                              String replyText) {
        super(authorId, tweetId);
        this.replyText = replyText;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public String getReplyText() {
        return replyText;
    }

}
