package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "TwitterActionDirectMessage")
public class TwitterActionDirectMessage extends AbstractNetworkUserAction<Twitter> implements NetworkUserAction<Twitter> {

    private String message;

    public TwitterActionDirectMessage() {
        super();
    }

    public TwitterActionDirectMessage(String authorId,
                                      String targetUserId,
                                      String message) {
        super(authorId, targetUserId);
        this.targetUserId = targetUserId;
        this.message = message;
    }

    public TwitterActionDirectMessage(String authorId,
                                      String targetUserId,
                                      String message,
                                      Date createdAt) {
        super(authorId, targetUserId);
        this.targetUserId = targetUserId;
        this.message = message;
        this.creationTimestamp = createdAt;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public String getMessage() {
        return message;
    }

}
