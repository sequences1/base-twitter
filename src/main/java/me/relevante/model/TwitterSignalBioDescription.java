package me.relevante.model;

import java.util.Date;

public class TwitterSignalBioDescription extends TwitterBaseSignal implements NetworkSignal {

    public TwitterSignalBioDescription() {
        super("BD", SignalCategory.SELF_DESCRIPTION);
    }

    public TwitterSignalBioDescription(TwitterProfile profile) {
        this();
        this.nlpAnalyzableContent = profile.getBioDescription();
        this.relatedEntityId = profile.getId();
        this.relatedUserId = profile.getId();
        this.signalTimestamp = new Date();
    }
}
