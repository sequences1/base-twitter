package me.relevante.model;

import me.relevante.network.Network;
import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class TwitterReply implements NetworkEntity {

    private long authorId;
    private long tweetId;
    private String replyText;
    private Date creationTimestamp;

    private TwitterReply() {
    }

    public TwitterReply(long authorId,
                        long tweetId,
                        String replyText,
                        Date creationTimestamp) {
        this.authorId = authorId;
        this.tweetId = tweetId;
        this.replyText = replyText;
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public Network getNetwork() {
        return Twitter.getInstance();
    }

    public String getAuthorId() {
        return String.valueOf(authorId);
    }

    public String getPostId() {
        return String.valueOf(tweetId);
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    public String getReplyText() {
        return replyText;
    }

    public Long getAuthorTwitterId() {
        return authorId;
    }

    public Long getPostTwitterId() {
        return tweetId;
    }

}
