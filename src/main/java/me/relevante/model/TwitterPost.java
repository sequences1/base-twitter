package me.relevante.model;

import me.relevante.network.Twitter;
import me.relevante.nlp.core.Keyword;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TwitterPost implements NetworkPost<Twitter, TwitterPost, TwitterProfile> {

    protected Long id;
    protected String authorId;
    protected int latitude;
    protected int longitude;
    protected String language;
    protected String text;
    protected String tweetUrl;
    protected Date creationTimestamp;

    @Transient
    protected TwitterProfile author;
    @Transient
    protected List<TwitterFav> favs;
    @Transient
    protected List<TwitterRetweet> retweets;
    @Transient
    protected List<TwitterReply> replies;
    @Transient
    protected List<Keyword> keywords;

    public TwitterPost() {
        favs = new ArrayList<>();
        retweets = new ArrayList<>();
        replies = new ArrayList<>();
        keywords = new ArrayList<>();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    @Override
    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    @Override
    public TwitterProfile getAuthor() {
        return this.author;
    }

    @Override
    public void setAuthor(TwitterProfile author) {
        this.author = author;
        this.authorId = (author != null) ? String.valueOf(author.getTwitterId()) : null;
    }

    public List<Keyword> getKeywords() {
        return new ArrayList<>(keywords);
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(this.text) + ". ";
        return analyzableContent;
    }

    @Override
    public void completeDataFrom(TwitterPost post) {
        updateDataWithReferenceObject(this, post);
    }

    @Override
    public void updateDataFrom(TwitterPost post) {
        updateDataWithReferenceObject(post, post);
    }

    public Long getTwitterId() {
        return id;
    }

    public void setTwitterId(Long twitterId) {
        this.id = twitterId;
    }

    public String getAuthorTwitterId() {
        return authorId;
    }

    public void setAuthorTwitterId(String authorTwitterId) {
        this.authorId = authorTwitterId;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTweetUrl() {
        return tweetUrl;
    }

    public void setTweetUrl(String tweetUrl) {
        this.tweetUrl = tweetUrl;
    }

    public void updateDataWithReferenceObject(TwitterPost referenceObject,
                                              TwitterPost post) {
        if (referenceObject.id != null) id = post.id;
        if (referenceObject.authorId != post.authorId) authorId = post.authorId;
        if (referenceObject.latitude != post.latitude) latitude = post.latitude;
        if (referenceObject.longitude != post.longitude) longitude = post.longitude;
        if (!StringUtils.isBlank(referenceObject.language)) language = post.language;
        if (!StringUtils.isBlank(referenceObject.text)) text = post.text;
        if (!StringUtils.isBlank(referenceObject.tweetUrl)) tweetUrl = post.tweetUrl;
        if (referenceObject.creationTimestamp != null) creationTimestamp = post.creationTimestamp;
    }

    public List<TwitterFav> getFavs() {
        return favs;
    }

    public void setFavs(List<TwitterFav> favs) {
        this.favs = favs;
    }

    public List<TwitterRetweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(List<TwitterRetweet> retweets) {
        this.retweets = retweets;
    }

    public List<TwitterReply> getReplies() {
        return replies;
    }

    public void setReplies(List<TwitterReply> replies) {
        this.replies = replies;
    }

    public TwitterReply findReplyByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterReply reply : this.replies) {
            if (authorId.equals(reply.getAuthorId())) {
                return reply;
            }
        }
        return null;
    }

    public TwitterRetweet findRetweetByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterRetweet retweet : this.retweets) {
            if (authorId.equals(retweet.getAuthorId())) {
                return retweet;
            }
        }
        return null;
    }

    public TwitterFav findFavByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterFav fav : this.favs) {
            if (authorId.equals(fav.getAuthorId())) {
                return fav;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "TwitterPost{" +
                "id=" + id +
                ", authorId=" + authorId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", language='" + language + '\'' +
                ", text='" + text + '\'' +
                ", tweetUrl='" + tweetUrl + '\'' +
                ", creationTimestamp=" + creationTimestamp +
                ", favs=" + favs +
                ", retweets=" + retweets +
                ", replies=" + replies +
                ", author=" + author +
                '}';
    }
}
