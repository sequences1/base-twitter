package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "TwitterFullUser")
public class TwitterFullUser implements NetworkFullUser<Twitter, TwitterProfile>, NetworkPostsContainer<Twitter, TwitterFullPost> {

    @Id
    private String id;
    private TwitterProfile profile;
    private List<TwitterFullPost> posts;
    private List<String> relatedTerms;
    private List<TwitterActionFollow> follows;
    private List<TwitterActionDirectMessage> directMessages;
    private List<String> postIds;

    public TwitterFullUser() {
        this.posts = new ArrayList<>();
        this.relatedTerms = new ArrayList<>();
        this.follows = new ArrayList<>();
        this.directMessages = new ArrayList<>();
        this.postIds = new ArrayList<>();
    }

    public TwitterFullUser(TwitterProfile profile) {
        this();
        this.profile = profile;
        this.id = profile.getId();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public TwitterProfile getProfile() {
        return profile;
    }

    @Override
    public List<String> getRelatedTerms() {
        return relatedTerms;
    }

    @Override
    public String getUserId() {
        return profile.getId();
    }

    @Override
    public List<TwitterFullPost> getLastPosts() {
        return posts;
    }

    public List<TwitterActionFollow> getFollows() {
        return follows;
    }

    public List<TwitterActionDirectMessage> getDirectMessages() {
        return directMessages;
    }

    public List<String> getPostIds() {
        return postIds;
    }

    public List<TwitterActionFollow> findFollowsByAuthorId(String authorId) {
        List<TwitterActionFollow> follows = new ArrayList<>();
        if (authorId == null) {
            return follows;
        }
        for (TwitterActionFollow follow : this.follows) {
            if (authorId.equals(follow.getAuthorId())) {
                follows.add(follow);
            }
        }
        return follows;
    }

    public List<TwitterActionDirectMessage> findDirectMessagesByAuthorId(String authorId) {
        List<TwitterActionDirectMessage> directMessages = new ArrayList<>();
        if (authorId == null) {
            return directMessages;
        }
        for (TwitterActionDirectMessage directMessage : this.directMessages) {
            if (authorId.equals(directMessage.getAuthorId())) {
                directMessages.add(directMessage);
            }
        }
        return directMessages;
    }
}
