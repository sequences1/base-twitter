package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterActionRetweet")
public class TwitterActionRetweet extends AbstractNetworkPostAction<Twitter> implements NetworkPostAction<Twitter> {

    private TwitterActionRetweet() {
        super();
    }

    public TwitterActionRetweet(String authorId,
                                String tweetId) {
        super(authorId, tweetId);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

}
