package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "TwitterSearchUser")
public class TwitterSearchUser implements NetworkSearchUser<Twitter, TwitterFullUser, TwitterFullPost> {

    @Id
    private String id;
    private List<NetworkSignal<Twitter>> signals;

    @Transient private TwitterFullUser fullUser;
    @Transient private List<TwitterFullPost> fullPosts;

    public TwitterSearchUser(TwitterFullUser fullUser) {
        this();
        this.id = fullUser.getId();
        this.fullUser = fullUser;
    }

    private TwitterSearchUser() {
        this.signals = new ArrayList<>();
        this.fullPosts = new ArrayList<>();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getUserId() {
        return getId();
    }

    @Override
    public TwitterFullUser getFullUser() {
        return fullUser;
    }

    @Override
    public void setFullUser(TwitterFullUser fullUser) {
        this.fullUser = fullUser;
    }

    @Override
    public List<NetworkSignal<Twitter>> getSignals() {
        return signals;
    }

    @Override
    public void addSignal(NetworkSignal signal) {
        if (signals.contains(signal)) {
            return;
        }
        this.signals.add(signal);
    }

    @Override
    public List<TwitterFullPost> getPosts() {
        return fullPosts;
    }

}
