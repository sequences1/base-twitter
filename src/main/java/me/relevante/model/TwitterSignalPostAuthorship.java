package me.relevante.model;

import java.util.Date;

public class TwitterSignalPostAuthorship extends TwitterBaseSignal implements NetworkSignal {

    public TwitterSignalPostAuthorship() {
        super("PA", SignalCategory.CONTENT_ACTIVITY);
    }

    public TwitterSignalPostAuthorship(TwitterPost post) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = post.getAuthorId();
        this.signalTimestamp = (post.getCreationTimestamp() != null) ? post.getCreationTimestamp() : new Date();
    }
}
