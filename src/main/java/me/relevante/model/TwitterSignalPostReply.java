package me.relevante.model;

import java.util.Date;

public class TwitterSignalPostReply extends TwitterBaseSignal implements NetworkSignal {

    public TwitterSignalPostReply() {
        super("PR", SignalCategory.CONTENT_ACTIVITY);
    }

    public TwitterSignalPostReply(TwitterPost post,
                                  String commentAuthorId,
                                  Date commentTimestamp) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = commentAuthorId;
        this.signalTimestamp = (commentTimestamp != null) ? commentTimestamp : new Date();
    }
}
