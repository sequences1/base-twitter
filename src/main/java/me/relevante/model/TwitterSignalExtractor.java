package me.relevante.model;

import me.relevante.network.Twitter;
import me.relevante.nlp.core.Keyword;
import me.relevante.nlp.core.NlpCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class TwitterSignalExtractor implements NetworkSignalExtractor<Twitter, TwitterFullUser, TwitterFullPost> {

    @Autowired private NlpCore nlpCore;

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public List<NetworkSignal<Twitter>> extract(TwitterFullUser fullUser) {

        Set<NetworkSignal<Twitter>> signals = new HashSet<>();
        List<NetworkSignal<Twitter>> profileSignals = extract(fullUser.getProfile());
        signals.addAll(profileSignals);

        return new ArrayList<>(signals);
    }

    @Override
    public List<NetworkSignal<Twitter>> extract(TwitterFullPost fullPost) {

        Set<NetworkSignal<Twitter>> signals = new HashSet<>();

        if (fullPost.getPost().getAuthorId() != null) {
            TwitterSignalPostAuthorship signal = new TwitterSignalPostAuthorship(fullPost.getPost());
            addSignal(signal, signals);
        }
        for (TwitterActionReply reply : fullPost.getReplies()) {
            TwitterSignalPostReply signal = new TwitterSignalPostReply(fullPost.getPost(),
                    reply.getAuthorId(), reply.getCreationTimestamp());
            addSignal(signal, signals);
        }
        for (TwitterActionRetweet retweet : fullPost.getRetweets()) {
            TwitterSignalPostRetweet signal = new TwitterSignalPostRetweet(fullPost.getPost(),
                    retweet.getAuthorId(), retweet.getCreationTimestamp());
            addSignal(signal, signals);
        }
        for (TwitterActionFav fav : fullPost.getFavs()) {
            TwitterSignalPostFav signal = new TwitterSignalPostFav(fullPost.getPost(),
                    fav.getAuthorId(), fav.getCreationTimestamp());
            addSignal(signal, signals);
        }

        return new ArrayList<>(signals);
    }

    private List<NetworkSignal<Twitter>> extract(TwitterProfile profile) {
        Set<NetworkSignal<Twitter>> signals = new HashSet<>();

        if (profile.getDescription() != null) {
            TwitterSignalDescription signal = new TwitterSignalDescription(profile);
            addSignal(signal, signals);
        }
        if (profile.getBioDescription() != null) {
            TwitterSignalBioDescription signal = new TwitterSignalBioDescription(profile);
            addSignal(signal, signals);
        }

        return new ArrayList<>(signals);
    }

    private void addSignal(NetworkSignal<Twitter> signal,
                           Collection<NetworkSignal<Twitter>> signals) {
        if (signals.contains(signal)) {
            return;
        }
        Map<String, Keyword> keywords = nlpCore.extractKeywords(signal.getNlpAnalyzableContent());
        signal.getContentStems().clear();
        signal.getContentStems().addAll(keywords.keySet());
        signals.add(signal);
    }

}
