package me.relevante.model;

import java.util.Date;

public class TwitterSignalDescription extends TwitterBaseSignal implements NetworkSignal {

    public TwitterSignalDescription() {
        super("D", SignalCategory.SELF_DESCRIPTION);
    }

    public TwitterSignalDescription(TwitterProfile profile) {
        this();
        this.nlpAnalyzableContent = profile.getDescription();
        this.relatedEntityId = profile.getId();
        this.relatedUserId = profile.getId();
        this.signalTimestamp = new Date();
    }
}
