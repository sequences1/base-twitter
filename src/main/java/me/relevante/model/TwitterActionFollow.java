package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterActionFollow")
public class TwitterActionFollow extends AbstractNetworkUserAction<Twitter> implements NetworkUserAction<Twitter> {

    public TwitterActionFollow() {
        super();
    }

    public TwitterActionFollow(String authorId,
                               String targetUserId) {
        super(authorId, targetUserId);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

}
