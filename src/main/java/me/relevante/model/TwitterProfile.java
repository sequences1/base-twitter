package me.relevante.model;

import me.relevante.network.Twitter;
import me.relevante.nlp.core.Keyword;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.List;

public class TwitterProfile implements NetworkProfile<Twitter, TwitterProfile> {

    @Id
    private Long id;
    private String name;
    private String screenName;
    private String location;
    private String description;
    private String profileUrl;
    private String imageUrl;
    private String bioDescription;
    private String bioUrl;
    private String followers;
    private String following;

    @Transient
    private List<Keyword> keywords;
    @Transient
    private List<TwitterActionFollow> follows;
    @Transient
    private List<TwitterActionDirectMessage> directMessages;

    public TwitterProfile() {
        this.follows = new ArrayList<>();
        this.directMessages = new ArrayList<>();
        this.keywords = new ArrayList<>();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getUserId() {
        return String.valueOf(id);
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(description) + ". " +
                StringUtils.defaultString(bioDescription) + ". ";
        return analyzableContent;
    }

    public Long getTwitterId() {
        return id;
    }

    public void setTwitterId(Long twitterId) {
        this.id = twitterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBioDescription() {
        return bioDescription;
    }

    public void setBioDescription(String bioDescription) {
        this.bioDescription = bioDescription;
    }

    public String getBioUrl() {
        return bioUrl;
    }

    public void setBioUrl(String bioUrl) {
        this.bioUrl = bioUrl;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public List<TwitterActionFollow> getFollows() {
        return follows;
    }

    public List<TwitterActionDirectMessage> getDirectMessages() {
        return directMessages;
    }

    @Override
    public void completeDataFrom(TwitterProfile user) {
        updateDataWithReferenceObject(this, user);
    }

    @Override
    public void updateDataFrom(TwitterProfile user) {
        updateDataWithReferenceObject(user, user);
    }

    private void updateDataWithReferenceObject(TwitterProfile referenceObject,
                                               TwitterProfile user) {
        if (referenceObject.id != null) id = user.id;
        if (!StringUtils.isBlank(referenceObject.name)) name = user.name;
        if (!StringUtils.isBlank(referenceObject.screenName)) screenName = user.screenName;
        if (!StringUtils.isBlank(referenceObject.location)) location = user.location;
        if (!StringUtils.isBlank(referenceObject.description)) description = user.description;
        if (!StringUtils.isBlank(referenceObject.profileUrl)) profileUrl = user.profileUrl;
        if (!StringUtils.isBlank(referenceObject.imageUrl)) imageUrl = user.imageUrl;
        if (!StringUtils.isBlank(referenceObject.bioDescription)) bioDescription = user.bioDescription;
        if (!StringUtils.isBlank(referenceObject.bioUrl)) bioUrl = user.bioUrl;
        if (!StringUtils.isBlank(referenceObject.followers)) followers = user.followers;
        if (!StringUtils.isBlank(referenceObject.following)) following = user.following;
    }

    public TwitterActionFollow findFollowByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterActionFollow follow : this.follows) {
            if (authorId.equals(follow.getAuthorId())) {
                return follow;
            }
        }
        return null;
    }

    public TwitterActionDirectMessage findDirectMessageByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterActionDirectMessage directMessage : this.directMessages) {
            if (authorId.equals(directMessage.getAuthorId())) {
                return directMessage;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "TwitterProfile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", screenName='" + screenName + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", profileUrl='" + profileUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", bioDescription='" + bioDescription + '\'' +
                ", bioUrl='" + bioUrl + '\'' +
                ", followers='" + followers + '\'' +
                ", following='" + following + '\'' +
                ", follows=" + follows +
                ", directMessages=" + directMessages +
                '}';
    }
}