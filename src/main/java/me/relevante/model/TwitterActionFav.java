package me.relevante.model;

import me.relevante.network.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterActionFav")
public class TwitterActionFav extends AbstractNetworkPostAction<Twitter> implements NetworkPostAction<Twitter> {

    private TwitterActionFav() {
        super();
    }

    public TwitterActionFav(String authorId,
                            String tweetId) {
        super(authorId, tweetId);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

}
