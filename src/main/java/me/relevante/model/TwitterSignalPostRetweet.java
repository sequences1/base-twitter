package me.relevante.model;

import java.util.Date;

public class TwitterSignalPostRetweet extends TwitterBaseSignal implements NetworkSignal {

    public TwitterSignalPostRetweet() {
        super("PRT", SignalCategory.CONTENT_ACTIVITY);
    }

    public TwitterSignalPostRetweet(TwitterPost post,
                                    String commentAuthorId,
                                    Date commentTimestamp) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = commentAuthorId;
        this.signalTimestamp = (commentTimestamp != null) ? commentTimestamp : new Date();
    }

}
